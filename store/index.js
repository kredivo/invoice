export const state = () => ({
  snack: '',
  loading: false,
  email_regexp: /^(([^<>()\]\\.,;:\s@"]+(\.[^<>()\]\\.,;:\s@"]+)*)|(".+"))@(([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  password_regexp: /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
  rules: {
    required: [v => !!v || 'Required'],
    email: [
      v => !!v || 'E-mail required',
      v => this.email_regexp.test(v) || 'E-mail must be valid'
    ],
    password: [
      v => !!v || 'Password required',
      v => this.password_regexp.test(v) || 'Minimum eight characters, at least one letter, one number, and one special character'
    ]
  }
})

export const mutations = {
  // Snackbar
  SET_SNACK (state, snack) {
    state.snack = snack
  },
  // End Snackbar
  SET_LOADING (state, value) {
    state.loading = value
  }
}

export const getters = {
}
