export const state = () => ({
  customer: {
    id: 1,
    name: 'Tdadasda',
    address: 'Jl. Csdasd',
    phone: '0851285128512',
    created_at: null,
    updated_at: null
  },
  customers: [
    {
      id: 1,
      name: 'Tdadasda',
      address: 'Jl. Csdasd',
      phone: '0851285128512',
      created_at: null,
      updated_at: null
    },
    {
      id: 2,
      name: 'Zsscasdasd',
      address: 'Jl. Asdasd',
      phone: '0854451285551',
      created_at: null,
      updated_at: null
    },
    {
      id: 3,
      name: 'Xczxcxzcz',
      address: 'Jl. Asdasd',
      phone: '0854451285551',
      created_at: null,
      updated_at: null
    }
  ],
  loading: false
})

export const mutations = {
  SET_LOADING (state, value) {
    state.loading = value
  },
  SET_CUSTOMER (state, value) {
    state.customer = value
  },
  SET_CUSTOMERS (state, value) {
    state.customers = value
  }
}

export const actions = {
  async GET_ALL ({ state, commit }, options) {
    commit('SET_LOADING', true)
    await this.$axios
      .get('customers')
      .then((response) => {
        if (response.data.status === 'success') {
          commit('SET_CUSTOMERS', response.data.data)
        }
      })
      .catch((error) => {
        console.log(error)
      })
      .finally(() => {
        commit('SET_LOADING', false)
      })
  },
  async REPORTING ({ state, commit }, id) {
    commit('SET_LOADING', true)
    await this.$axios
      .get('customers/reporting')
      .then((response) => {
        if (response.data.status === 'success') {
          commit('SET_CUSTOMERS', response.data.data)
        }
      })
      .catch((error) => {
        const message = error.response.data.message ? error.response.data.message : 'Connection error, please try again'
        commit('SET_SNACK', message, { root: true })
      })
      .finally(() => {
        commit('SET_LOADING', false)
      })
  }
}
