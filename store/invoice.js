export const state = () => ({
  invoice: {
    id: 1,
    date: '2020-05-02',
    sub_total: 310.00,
    tax: 30.64,
    total: 336.40,
    customer_name: 'Ervan',
    customer_address: 'Hello',
    customer_phone: '081288881234',
    items: [
      {
        id: 1,
        name: 'Cut off machine',
        qty: 1,
        unit_price: 245.50,
        amount: 245.50
      }
    ],
    created_at: null,
    updated_at: null
  },
  invoices: [
    {
      id: 1,
      date: '2020-05-02',
      total_amount: 310.00,
      customer_name: 'Ervan',
      customer_address: 'Hello',
      items: [],
      created_at: null,
      updated_at: null
    }
  ],
  loading: false
})

export const mutations = {
  SET_LOADING (state, value) {
    state.loading = value
  },
  SET_INVOICE (state, value) {
    state.invoice = value
  },
  SET_INVOICES (state, value) {
    state.invoices = value
  },
  DELETE_INVOICE (state, id) {
    state.invoices = state.invoices.filter((value, index) => { return id !== value.id })
  }
}

export const actions = {
  async GET_ALL ({ state, commit }, options) {
    commit('SET_LOADING', true)

    await this.$axios
      .get('invoices')
      .then((response) => {
        if (response.data.status === 'success') {
          commit('SET_INVOICES', response.data.data)
          commit('SET_SNACK', response.data.message, { root: true })
        }
      })
      .catch((error) => {
        const message = error.response.data.message ? error.response.data.message : 'Connection error, please try again'
        commit('SET_SNACK', message, { root: true })
      })
      .finally(() => {
        commit('SET_LOADING', false)
      })
  },
  async GET ({ state, commit }, id) {
    commit('SET_LOADING', true)
    let serverResponse = null
    await this.$axios
      .get('invoices/' + id)
      .then((response) => {
        if (response.data.status === 'success') {
          commit('SET_INVOICE', response.data.data)
        }
        serverResponse = response ? response.data : response
      })
      .catch((error) => {
        const message = error.response.data.message ? error.response.data.message : 'Connection error, please try again'
        commit('SET_SNACK', message, { root: true })
      })
      .finally(() => {
        commit('SET_LOADING', false)
      })

    return serverResponse
  },
  async STORE ({ state, commit }, { customerId, invoiceItems }) {
    commit('SET_LOADING', true)
    let serverResponse = null
    await this.$axios
      .post('invoices', {
        customer_id: customerId,
        invoice_items: invoiceItems
      })
      .then((response) => {
        serverResponse = response.data
      })
      .catch((error) => {
        const message = error.response.data.message ? error.response.data.message : 'Connection error, please try again'
        commit('SET_SNACK', message, { root: true })
      })
      .finally(() => {
        commit('SET_LOADING', false)
      })

    return serverResponse
  },
  async UPDATE ({ state, commit }, { invoiceId, customerId, invoiceItems }) {
    commit('SET_LOADING', true)
    let serverResponse = null
    await this.$axios
      .put(`invoices/${invoiceId}`, {
        customer_id: customerId,
        invoice_items: invoiceItems
      })
      .then((response) => {
        serverResponse = response.data
      })
      .catch((error) => {
        const message = error.response.data.message ? error.response.data.message : 'Connection error, please try again'
        commit('SET_SNACK', message, { root: true })
      })
      .finally(() => {
        commit('SET_LOADING', false)
      })

    return serverResponse
  },
  async DELETE ({ state, commit }, id) {
    commit('SET_LOADING', true)
    await this.$axios
      .delete('invoices/' + id)
      .then((response) => {
        if (response.data.status === 'success') {
          commit('DELETE_INVOICE', id)
          commit('SET_SNACK', response.data.message, { root: true })
        }
      })
      .catch((error) => {
        const message = error.response.data.message ? error.response.data.message : 'Connection error, please try again'
        commit('SET_SNACK', message, { root: true })
      })
      .finally(() => {
        commit('SET_LOADING', false)
      })
  }
}
