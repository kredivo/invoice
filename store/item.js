export const state = () => ({
  items: [
    {
      id: 1,
      name: 'Item 1',
      unit_price: 203.11,
      created_at: null,
      updated_at: null
    },
    {
      id: 2,
      name: 'Item 2',
      unit_price: 111.11,
      created_at: null,
      updated_at: null
    },
    {
      id: 3,
      name: 'Item 3',
      unit_price: 22.11,
      created_at: null,
      updated_at: null
    }
  ],
  loading: false
})

export const mutations = {
  SET_LOADING (state, value) {
    state.loading = value
  },
  SET_ITEMS (state, value) {
    state.items = value
  }
}

export const actions = {
  async GET_ALL ({ state, commit }, options) {
    commit('SET_LOADING', true)
    await this.$axios
      .get('items')
      .then((response) => {
        if (response.data.status === 'success') {
          commit('SET_ITEMS', response.data.data)
        }
      })
      .catch((error) => {
        const message = error.response.data.message ? error.response.data.message : 'Connection error, please try again'
        commit('SET_SNACK', message, { root: true })
      })
      .finally(() => {
        commit('SET_LOADING', false)
      })
  }
}
