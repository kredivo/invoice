import Vue from 'vue'

Vue.mixin({
  methods: {
    // Force user to input number / arrow on keyboard / backspace / delete only
    onlyNumber ($event) {
      const keyCode = ($event.keyCode ? $event.keyCode : $event.which)
      if (keyCode === 8 || keyCode === 46 || (keyCode >= 48 && keyCode <= 57) || keyCode === 9 || keyCode === 13 || keyCode === 17 || (keyCode >= 37 && keyCode <= 40)) {

      } else { $event.preventDefault() }
    },

    // Format date to specific format using DayJS
    formatDate (date, format = 'MMMM, D YYYY HH:mm:ss') {
      return date ? this.$dayjs(date).format(format) : '-'
    },

    // Format currency to US Dollar
    formatCurrency (value) {
      const val = (value / 1).toFixed(2).replace('.', ',')
      return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
    }
  }
})
