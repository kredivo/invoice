export default function ({ $auth, store, redirect, routes }) {
  // Fetch logged in user
  const user = $auth.user
  // If the user is not authenticated
  if (!user) {
    redirect('/login')
  }
}
